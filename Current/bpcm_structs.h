// struct matrix_nbyn{
//      int n;
//      double *m;
// };

struct spline_data{
     int n;
     double *x;
     double *y;     
     double *h;
     double *z;
     double *s1;
     double **m;
     double **u;     
};

struct data_struct_xy{
         int n;
         double *X;	//
         double *Y;
};


struct ang_dist{
         double sum_y_hist_mu;
         struct data_struct_xy hist_mu;
	 struct data_struct_xy F_theta;
	 struct data_struct_xy F_mu;
};

struct E_dist{
         double sum_y_hist_E;
         double M1_E;
         double M1_L;
         struct data_struct_xy hist_E;
	 struct data_struct_xy F_E;
	 struct data_struct_xy F_L;
};


// cylindric shell detector data structure
struct CYLDET{
  
  int nphi;
  int nh;
  double *data;
};


// plane detector data structure
struct PLDET{
  
  int nx;
  int ny;
  double *data;
};


// implement structure to pass data from source to detector (for Adjoint Current Detector)
struct Data_Source{
  
  double *dat;
};


// xy vs lambda histogram structure data detector
struct XYLH{
  
  int nx;
  int ny;
  int nlambda;
  double *data;
  
};