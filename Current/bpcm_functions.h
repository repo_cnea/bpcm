// init XYLH detector - allocate memory
void init_XYLH( struct XYLH *det);

        void init_XYLH( struct XYLH *det){
	  
	(*det).data = malloc((((*det).nx)*((*det).ny)*((*det).nlambda)+1) * sizeof(double));       
        if ((*det).data==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        
         int i;
 	
        for(i = 0; i<((*det).nx)*((*det).ny)*((*det).nlambda); i++){
         
                 (*det).data[i]=0.0;
         }	
	  
	}
// end init_pldet function	



void init_cyl_det( struct CYLDET *det);

        void init_cyl_det( struct CYLDET *det){
	  
	(*det).data = malloc((((*det).nphi)*((*det).nh)+1) * sizeof(double));       
        if ((*det).data==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        
         int i;
 	
        for(i = 0; i<((*det).nphi)*((*det).nh); i++){
         
                 (*det).data[i]=0.0;
         }	
	  
	}
// end ini_cyl_det function	



void init_pldet( struct PLDET *det);

        void init_pldet( struct PLDET *det){
	  
	(*det).data = malloc((((*det).nx)*((*det).ny)+1) * sizeof(double));       
        if ((*det).data==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        
         int i;
 	
        for(i = 0; i<((*det).nx)*((*det).ny); i++){
         
                 (*det).data[i]=0.0;
         }	
	  
	}
// end init_pldet function	


void init_Source_Data( struct Data_Source *det);

        void init_Source_Data( struct Data_Source *det){
	  
	(*det).dat = malloc((2) * sizeof(double));       
        if ((*det).dat==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        
         int i;
 	
        for(i = 0; i<2; i++){
         
                 (*det).dat[i]=0.0;
         }	
	  
	}
// end init_pldet function	


void read_file(char *filename, struct data_struct_xy *hist_mu);

        void read_file(char *filename, struct data_struct_xy *hist_mu){

        int i,j,n;
        int result_comp_1=1;
        int result_comp_2=1;
        int result_comp_3=1;
        char line[300];
        int c;
        FILE * filei;
        double x,y,z;
        int N;
        double sum;

        n=0; 
        filei = fopen (filename,"r+");
    
        while(c != EOF){
                c=fscanf(filei," %[^\n]",line);
                result_comp_1=strncmp(line,"//",2);
                if((result_comp_1 !=0)&&(c != EOF)){
                        n++;
                }
        }
    
     
        N=n-1;   
        
        (*hist_mu).X = malloc((N+1) * sizeof(double));
        (*hist_mu).Y = malloc((N+1) * sizeof(double));
        if ((*hist_mu).X==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*hist_mu).Y==NULL){
        printf("Error allocating memory!\n");
        exit(0);}

        
        rewind (filei);
        n=0; 
        c=0;
        sum=0;
        (*hist_mu).n=N+1;        
 
        while(c != EOF){
                c=fscanf(filei," %[^\n]", line);
                result_comp_1=strncmp(line,"//",2);      
                if((result_comp_1 !=0)&&(c != EOF)){
                        sscanf(line,"%lf %lf",&x,&y);
                        (*hist_mu).X[n]=x;
                        (*hist_mu).Y[n]=y;
                        n++;             
                }
        };
    
        fclose(filei);
        return;  
        }
// end read_file function
        



// begin read_ang_dist function
void read_ang_dist(char *filename, struct ang_dist *ANG_DIST);

        void read_ang_dist(char *filename, struct ang_dist *ANG_DIST){
  
        int i;
        double sum=0;
        double dummy=0.0;  
        read_file(filename,&((*ANG_DIST).hist_mu));
        int n=(*ANG_DIST).hist_mu.n;
        (*ANG_DIST).F_theta.n=n;
        (*ANG_DIST).F_mu.n=n;
    
        for(i = 0; i<n; i++){
                sum+=(*ANG_DIST).hist_mu.Y[i];      
        }

        (*ANG_DIST).sum_y_hist_mu=sum;
    
        (*ANG_DIST).F_theta.X = malloc((n+1) * sizeof(double));
        (*ANG_DIST).F_theta.Y = malloc((n+1) * sizeof(double));
        (*ANG_DIST).F_mu.X = malloc((n+1) * sizeof(double));
        (*ANG_DIST).F_mu.Y = malloc((n+1) * sizeof(double));        
        if ((*ANG_DIST).F_theta.X==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*ANG_DIST).F_theta.Y==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*ANG_DIST).F_mu.X==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*ANG_DIST).F_mu.Y==NULL){
        printf("Error allocating memory!\n");
        exit(0);}        
     
        for(i = 0; i<n; i++){
                (*ANG_DIST).F_theta.X[i]=acos((*ANG_DIST).hist_mu.X[n-1-i]);
                (*ANG_DIST).F_theta.Y[i]=dummy;
                dummy=dummy+(*ANG_DIST).hist_mu.Y[n-1-i]/sum;
        }
   
        dummy=0.0;
        for(i = 0; i<n; i++){
                (*ANG_DIST).F_mu.X[i]=(*ANG_DIST).hist_mu.X[i];
                (*ANG_DIST).F_mu.Y[i]=(*ANG_DIST).hist_mu.Y[i]/sum+dummy;
                dummy=(*ANG_DIST).F_mu.Y[i];
        }

        return;
        }
// end read_ang_dist function



// begin read_E_dist
void read_E_dist(char *filename, struct E_dist *EDIST);

        void read_E_dist(char *filename, struct E_dist *EDIST){
  
        int i;
        double sum=0;
        double dummy=0.0; 
        double factor=8.18042067592E+01; //--> E=factor/L/L   (factor=h*h/2/m_n [meV*AA*AA]
        double cut_factor=1.0/100.0; // cut factor for a bin including zero energy
        read_file(filename,&((*EDIST).hist_E));
        int n=(*EDIST).hist_E.n;
        (*EDIST).F_E.n=n;
        (*EDIST).F_L.n=n;
 
        for(i = 0; i<n; i++){
                sum+=(*EDIST).hist_E.Y[i];      
        }

        (*EDIST).sum_y_hist_E=sum;
        
        (*EDIST).F_E.X = malloc((n+1) * sizeof(double));
        (*EDIST).F_E.Y = malloc((n+1) * sizeof(double));
        (*EDIST).F_L.X = malloc((n+1) * sizeof(double));
        (*EDIST).F_L.Y = malloc((n+1) * sizeof(double));        
        if ((*EDIST).F_E.X==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*EDIST).F_E.Y==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*EDIST).F_L.X==NULL){
        printf("Error allocating memory!\n");
        exit(0);}
        if ((*EDIST).F_L.Y==NULL){
        printf("Error allocating memory!\n");
        exit(0);}

     
        for(i = 0; i<n; i++){
                if((*EDIST).hist_E.X[n-1-i] !=0){ 
                        (*EDIST).F_L.X[i]=sqrt(factor/((*EDIST).hist_E.X[n-1-i]*1E6*1E3));
                        (*EDIST).F_L.Y[i]=dummy;
                        dummy=dummy+(*EDIST).hist_E.Y[n-1-i]/sum;
                        }else{
                                (*EDIST).F_L.X[i]=sqrt(factor/
                                ((
                                 (*EDIST).hist_E.X[n-0-i]
                                -(*EDIST).hist_E.X[n-1-i]
                                  )*1E6*1E3*cut_factor));
                                
                                (*EDIST).F_L.Y[i]=dummy-(*EDIST).hist_E.Y[n-0-i]/sum*cut_factor;
                                dummy=dummy+(*EDIST).hist_E.Y[n-1-i]/sum;
                }
        }
   
        dummy=0.0;
        for(i = 0; i<n; i++){
                (*EDIST).F_E.X[i]=(*EDIST).hist_E.X[i]*1E6*1E3;
                (*EDIST).F_E.Y[i]=(*EDIST).hist_E.Y[i]/sum+dummy;
                dummy=(*EDIST).F_E.Y[i];
        }
   
        dummy=0.0;
        for(i = 0; i<n-1; i++){   
                dummy=dummy+((*EDIST).F_E.Y[i]+(*EDIST).F_E.Y[i+1])*((*EDIST).F_E.X[i+1]-(*EDIST).F_E.X[i])/2.0;
        }
        (*EDIST).M1_E=(*EDIST).F_E.X[n-1]-dummy;

        dummy=0.0;
        for(i = 0; i<n-1; i++){   
                dummy=dummy+((*EDIST).F_L.Y[i]+(*EDIST).F_L.Y[i+1])*((*EDIST).F_L.X[i+1]-(*EDIST).F_L.X[i])/2.0;
        }
        (*EDIST).M1_L=(*EDIST).F_L.X[n-1]-dummy;
   
        return;
        }
// end read_E_dist


// begin interpolate_F_x
double interpolate_F_x(struct data_struct_xy *F_XY, double x);

        double interpolate_F_x(struct data_struct_xy *F_XY, double x){
                int i=0;
                int n=(*F_XY).n;
                double val=0.0;
                
                if(x>(*F_XY).X[0]){
                                if(x<(*F_XY).X[n-1]){
                                        while(x>=(*F_XY).X[i]){
                                                i++;                                                
                                        }
                                        val=(*F_XY).Y[i-1]+(x-(*F_XY).X[i-1])*((*F_XY).Y[i]-(*F_XY).Y[i-1])/((*F_XY).X[i]-(*F_XY).X[i-1]); 
                                        }else{
                                                val=(*F_XY).Y[n-1];         
                                }
                                
                }
                return val;
         }
// end interpolate_F_x



// begin interpolate_Finv_y
double interpolate_Finv_y(struct data_struct_xy *F_XY, double y);

        double interpolate_Finv_y(struct data_struct_xy *F_XY, double y){

                int i=0;
                int n=(*F_XY).n;
                double val=0.0;

                if(y>(*F_XY).Y[0]){
                                if(y<(*F_XY).Y[n-1]){
                                        while(y>=(*F_XY).Y[i]){
                                                i++;                                                
                                        }
                                        val=(*F_XY).X[i-1]+(y-(*F_XY).Y[i-1])*((*F_XY).X[i]-(*F_XY).X[i-1])/((*F_XY).Y[i]-(*F_XY).Y[i-1]); 
                                        }else{
                                                val=(*F_XY).X[n-1];         
                                }
                                
                }

                return val;
        }
// end interpolate_Finv_y


// begin BS_Method_root
double BS_Method_root(double a, double b, double f_x, double (*f)(double), double rel_err );
     
     double BS_Method_root(double a, double b, double f_x, double (*f)(double), double rel_err ){
     double val;
     double error;
     int i=0;     
     if((*f)(a)>=f_x){
          val=a;
          }else if ((*f)(b)<=f_x){
                    val=b;
                    }else if ((f_x-(*f)(a))*(f_x-(*f)(b)) < 0){
                         val=(a+b)/2;                         
                         error=fabs((a-b)/2.0);
                         while(rel_err*val<error){
                              
                              if((f_x-(*f)(a))*(f_x-(*f)(val))>0){                                   
                                   a=val;
                                   }else{
                                        b=val;     
                              }
                              error=fabs((a-b)/2.0);
                              val=(a+b)/2;
                               i++;
                         }                                                                      
                         }else{
                              printf("error on BS_Method_root function (root out of specified range)\n");
                              exit(0);
     }
     return val;
     }
// end BS_Method_root


// begin spline_calc
void spline_calc(struct data_struct_xy F , struct spline_data *S);

     void spline_calc(struct data_struct_xy F, struct spline_data *S){
          int i,j;
          int n=F.n;
          (*S).n=n;
          double coeff;
          
          (*S).h = malloc( n * sizeof(double));             // allocate memory for x,y, h, z and s1
          (*S).x = malloc((n+1) * sizeof(double));
          (*S).y = malloc((n+1) * sizeof(double));                    
          (*S).z = malloc((n+1) * sizeof(double));
          (*S).s1 = malloc((n+1) * sizeof(double));          
          if ((*S).h==NULL){
               printf("Error allocating memory!\n");
               exit(0);               
          }
          if ((*S).x==NULL){
               printf("Error allocating memory!\n");
               exit(0);               
          }          
          if ((*S).y==NULL){
               printf("Error allocating memory!\n");
               exit(0);               
          }                    
          if ((*S).z==NULL){
               printf("Error allocating memory!\n");
               exit(0);
          }
          if ((*S).s1==NULL){
               printf("Error allocating memory!\n");
               exit(0);
          }                    
          
          (*S).m=malloc(n * sizeof(double *));              // allocate memory for matrix m (n by (n+1))
          if ((*S).m==NULL){
          printf("Error allocating memory!\n");
          exit(0);}          
          for(i = 0; i<n; i++){
               (*S).m[i]=malloc((n+1) * sizeof(double));
               if ((*S).m==NULL){
                    printf("Error allocating memory!\n");
                    exit(0);
               }
          }                    

         
          for(i = 0; i<n; i++){                               // set xi
               (*S).x[i]=F.X[i];
          }
          

          for(i = 0; i<n; i++){                               // set yi
               (*S).y[i]=F.Y[i];
          }          

          for(i = 0; i<n-1; i++){                               // set h1
               (*S).h[i]=F.X[i+1]-F.X[i];
          }
          

          for(i = 0; i<n; i++){                                 // clean matrix m values                                     
               for(j = 0; j<n+1; j++){
                    (*S).m[i][j]=0.0;
               }
          }

          for(i = 0; i<n-2; i++){                                // set matrix m
               (*S).m[i+1][i]=(*S).h[i];     
               (*S).m[i+1][i+1]=2*((*S).h[i]+(*S).h[i+1]);
               (*S).m[i+1][i+2]=(*S).h[i+1];
               (*S).m[i+1][n]=6*(
                    (F.Y[i+2]-F.Y[i+1])/(*S).h[i+1]
                    -(F.Y[i+1]-F.Y[i])/(*S).h[i]
                              );                              
          }
          (*S).m[0][0]=-(*S).h[0]/3;
          (*S).m[0][1]=-(*S).h[0]/6;
          (*S).m[n-1][n-2]=(*S).h[n-2]/6;
          (*S).m[n-1][n-1]=(*S).h[n-2]/3;
          (*S).m[0][n]=(F.Y[0]-F.Y[1])/(*S).h[0]; 
          (*S).m[n-1][n]=(F.Y[n-2]-F.Y[n-1])/(*S).h[n-2];          


          
          for(i = 1; i<n; i++){                               // transform matrix m to upper diagonal
                  coeff=(*S).m[i][i-1]/(*S).m[i-1][i-1];                  
               for(j = 0; j<n+1; j++){                                                     
                    (*S).m[i][j]=(*S).m[i][j]
                    -coeff*(*S).m[i-1][j];     
               }               
          }
                             

          (*S).z[n-1]=(*S).m[n-1][n]/(*S).m[n-1][n-1];          // solve zi
          for(j = 0; j<n-1; j++){                               
               i=n-2-j;
               (*S).z[i]=((*S).m[i][n]-(*S).m[i][i+1]*(*S).z[i+1])/(*S).m[i][i];
          }          

          for(i = 0; i<n-1; i++){                               // set s1_i
               (*S).s1[i]=(F.Y[i+1]-F.Y[i])/(*S).h[i]-(*S).z[i+1]*(*S).h[i]/6.0-(*S).z[i]*(*S).h[i]/3.0;
          }          
                   
          return;
     }
// end spline_calc



// begin spline
double spline(double x, struct spline_data *S);
     
     double spline(double x, struct spline_data *S){
             int i=0;
             double val;
             int n=(*S).n;
      
             if(x<=(*S).x[0]){
                val=(*S).y[0];
                }else if (x>(*S).x[n-1]){
                        val=(*S).y[n-1];

                        }else{
                                while(x>(*S).x[i]){
                                        i++;                                              
                                }
                                i=i-1;
                                val=(*S).y[i]+(*S).s1[i]*(x-(*S).x[i])+(*S).z[i]/2*(x-(*S).x[i])*(x-(*S).x[i])+((*S).z[i+1]-(*S).z[i])/6.0/(*S).h[i]*(x-(*S).x[i])*(x-(*S).x[i])*(x-(*S).x[i]);
                                
                                
             }
             return val;
     }
// end spline



// begin BS_Method_root_spline
double BS_Method_root_spline(double a, double b, double f_x, struct spline_data *S, double rel_err );
     
     double BS_Method_root_spline(double a, double b, double f_x, struct spline_data *S, double rel_err ){
     double val;
     double error;
     int i=0;     
     if(spline(a,S)>=f_x){
          val=a;
          }else if (spline(b,S)<=f_x){
                    val=b;
                    }else if ((f_x-spline(a,S))*(f_x-spline(b,S)) < 0){
                         val=(a+b)/2;                         
                         error=fabs((a-b)/2.0);
                         while(rel_err*val<error){
                              
                              if((f_x-spline(a,S))*(f_x-spline(val,S))>0){                                   
                                   a=val;
                                   }else{
                                        b=val;     
                              }
                              error=fabs((a-b)/2.0);
                              val=(a+b)/2;
                               i++;
                         }                                                                      
                         }else{
                              printf("error on BS_Method_root function (root out of specified range)\n");
                              exit(0);
     }
     return val;
     }
// end BS_Method_root_spline


