double phi_var(double vx1, double vy1);
double phi_var(double vx1, double vy1){
        double val;
        val=atan(fabs(vx1)/fabs(vy1));
        if(vx1>0){
                 if (vy1>0){
                           val=val;
                           }else{
                           val=2*PI-val;      
                           }
                 }else{
                 if (vy1>0){
                           val=PI-val;
                           }else{
                           val=PI+val;      
                           }
                 }
        return val;
}

double phivar(double vx1, double vy1);
double phivar(double vx1, double vy1){
        double val;

    if (vx1>0){
    val=atan(vy1/vx1)*180/PI;
    }else{
    val=180+atan(vy1/vx1)*180/PI;  
    }    
    
        return val;
}

//THETA Distribution


// double FDC_theta(double x);
// double FDC_theta(double x){
//         double val=0.0;
//         
//         if((x>=0) && (x<=PI/2)){
//         val=(1-cos(2.0*x))/2.0; 
//         }
//         return val;
// }
// 
// double FDCinv_theta(double x);
// double FDCinv_theta(double x){
//         
//         double val=0.0;
//         if((x>=0) && (x<=1)){
//         val=acos(1-2*x)/2.0; 
//         }
//         return val; 
//  } 
 
double FDC_theta(double x);
double FDC_theta(double x){
        double val=0.0;
        
        if((x>0) && (x<=PI)){
        val=1.0;
        }
        return val;
}

double FDCinv_theta(double x);
double FDCinv_theta(double x){
        
        double val=0.0;
        if((x>=0) && (x<=1)){
        val=0.0; 
        }
        return val; 
 } 
 
 
double FDC_E(double x);
double FDC_E(double x){
 return 1-exp(-x*x/100/100); 
}

double FDCinv_E(double x);
double FDCinv_E(double x){
 return 100*sqrt(-log(1-x)); 
}


double FDC_const(double x);
double FDC_const(double x){
        double val=0.0;
        
        if((x>=0) && (x<=1)){
        val=x; 
        }
        return val;
}

double FDCinv_const(double x);
double FDCinv_const(double x){        
        double val=0.0;
        if((x>=0) && (x<=1)){
        val=x; 
        }
        return val; 
}
