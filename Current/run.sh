./clean.sh
mcstas bpcm_example.instr 
gcc -lm bpcm_example.c -o bpcm_example.exec
rm -r ./out
./bpcm_example.exec -d out -n 1e6 THETA_out=90.0 hm=1 km=1 lm=5 L0=4.0
mcplot out/mccode.sim
